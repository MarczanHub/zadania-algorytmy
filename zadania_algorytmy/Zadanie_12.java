package zadania_algorytmy;

import java.util.Arrays;

public class Zadanie_12 {

    // Napisz program, kt�ry wyszuka najwi�ksz� r�nic� pomi�dzy liczbami w tablicy
    //
    // bigDiff([10, 3, 5, 6]) ? 7
    // bigDiff([7, 2, 10, 9]) ? 8
    // bigDiff([2, 10, 7, 2]) ? 8

    public static void main(String[] args) {

        int[] bigDiff1 = {10, 3, 5, 6};
        int[] bigDiff2 = {7, 2, 10, 9};
        int[] bigDiff3 = {2, 10, 7, 2};

        int result1 = bigDiff(bigDiff1);
        int result2 = bigDiff(bigDiff2);
        int result3 = bigDiff(bigDiff3);

        System.out.println("bigDiff1: " + Arrays.toString(bigDiff1));
        System.out.println("Najwi�ksza r�nica pomi�dzy liczbami wynosi: " + result1 + "\n");
        System.out.println("bigDiff2: " + Arrays.toString(bigDiff2));
        System.out.println("Najwi�ksza r�nica pomi�dzy liczbami wynosi: " + result2 + "\n");
        System.out.println("bigDiff3: " + Arrays.toString(bigDiff3));
        System.out.println("Najwi�ksza r�nica pomi�dzy liczbami wynosi: " + result3);


    }
    public static int bigDiff (int[] numbers){

        int maxValue = numbers[0];
        int minValue = numbers[0];

        for (int i = 0; i < numbers.length; i++){
            maxValue = Math.max(maxValue, numbers[i]);
            minValue = Math.min(minValue, numbers[i]);
        }
        int result = maxValue - minValue;
        return result;
    }
}
