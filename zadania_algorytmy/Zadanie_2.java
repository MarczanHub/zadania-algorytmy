package zadania_algorytmy;

import java.util.Scanner;

public class Zadanie_2 {

    // Zaprogramuj algorytm sprawdzaj�cy, czy dana liczba jest pierwsza
    //
    // 7 to jest liczba pierwsza
    // 6 to nie jest liczba pierwsza

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("\nProgram sprawdzi czy dana liczba jest liczb� pierwsz�.\nPodaj liczb�:");
        int number = sc.nextInt();
        boolean isPrime = false;

        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                isPrime = true;
                break;
            }
        }
        if (!isPrime){
            System.out.println(number + " to jest liczba pierwsza.");
        }
        else{
            System.out.println(number + " to nie jest liczba pierwsza.");
        }
    }
}
