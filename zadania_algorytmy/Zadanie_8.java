package zadania_algorytmy;

import java.util.Scanner;

public class Zadanie_8 {

    // Zlicz wszystkie litery w podanym tek�cie, np. Ala ma kota -> 9

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Program obliczy liczb� liter w podanym tek�cie.");
        System.out.println("Wpisz wyraz lub ci�g wyraz�w:");
        String text = sc.nextLine();

        int letterCount = 0;
        for (int i = 0; i < text.length(); i++){
            if (Character.isLetter(text.charAt(i))){
                letterCount++;
            }
        }

        System.out.print("Liczba wszystkich liter w podanym tek�cie wynosi: " + letterCount);

    }
}
