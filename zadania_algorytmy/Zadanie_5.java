package zadania_algorytmy;

public class Zadanie_5 {

    // Wypisz liczby od 1 do 100, nast�pnie dla liczb podzielnych przez 3 wypisz Fizz, dla liczb podzielnych
    // przez 5 wypisz Buzz, dla liczb podzielnych przez 3 i 5 wypisz FizzBuzz.
    // W przeciwnym razie wypisz warto�� liczby.
    //
    // 1
    // 2
    // Fizz
    // 4
    // Buzz
    // ...
    // 13
    // 14
    // FizzBuzz
    // 16

    public static void main(String[] args) {

        for (int i = 1; i <= 100; i++){
            if (i % 3 == 0 && i % 5 == 0){
                System.out.println("FizzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }
}
