package zadania_algorytmy;

public class Zadanie_1 {

    // Zaprogramuj algorytm znajduj�cy najwi�kszy element z podanego ci�gu liczb

    public static void main(String[] args) {

        int[] numbers = {1, 13, 51322, -513, 0, 53, 100, 555};
        int printInt = najwiekszaLiczba(numbers);

        System.out.println("Najwi�ksza element to: " + printInt);

    }
    public static int najwiekszaLiczba (int[] liczby){

        int maxValue = 0;
        for (int i = 0; i < liczby.length; i++){
            if (liczby[i] > maxValue)
                maxValue = liczby[i];
        }

        return maxValue;
    }
}

