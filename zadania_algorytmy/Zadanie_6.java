package zadania_algorytmy;

import java.util.Scanner;

public class Zadanie_6 {

    //Sprawd�, czy podany wyraz jest palidromem czytany od przodu i od ty�u jest taki sam np. kajak, ala, oko

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Program sprawdzi czy podany wyraz jest palindromem");
        System.out.println("Je�li wyraz b�dzie palindromem, program zwr�ci warto�� 'true', w przeciwnym wypadku zwr�ci 'false'.");
        System.out.println("Podaj wyraz: ");
        System.out.println(isPalindrome(sc.nextLine()));
    }

    public static boolean isPalindrome(String word) {

        for (int i = 0; i < word.length() / 2; i++) {
            if (word.charAt(i) != word.charAt(word.length() - 1 - i)) {
                return false;
            }
        } return true;
    }
}