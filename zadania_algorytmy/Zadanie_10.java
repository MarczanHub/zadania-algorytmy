package zadania_algorytmy;

import java.util.Scanner;

public class Zadanie_10 {

    // Napisz program, kt�ry zlicza wszystkie liczby od 0 do N.
    // np. 6
    // 1+2+3+4+5+6 = 21
    // wynik 21

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int i = 0;
        int sum = 0;
        int number;

        System.out.println("Program zliczy wszystkie liczby od zera do podanej liczby.");
        System.out.println("Podaj liczb�:");
        number = sc.nextInt();

        while (i <= number) {
            sum += i;
            i++;
        }
        System.out.print("Suma = " + sum);
    }
}

