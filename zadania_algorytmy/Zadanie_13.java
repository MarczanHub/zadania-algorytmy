package zadania_algorytmy;

import java.util.Arrays;

public class Zadanie_13 {

    // Napisz program, kt�ry za pomoc� jednego przej�cia znajdzie wszystkie pary liczb,
    // kt�rych suma jest r�wna 0.
    //
    // numbers[] = {1,2,4,7,8,10,10,-1,-2,-4,100,22,-10}
    //
    // result[] = {-1,1 2,-2,4,-4,10,-10}

    static void printPairs(int[] numbers, int size) {
        int sum = 0;
        for (int i = 0; i < size; i++) {
            for (int j = i + 1; j < size; j++) {
                if (numbers[i] + numbers[j] == sum) {
                    int[] result = new int[]{numbers[i], numbers[j]};
                    System.out.print(Arrays.toString(result));

                    // Na pewno drukowanie 4 tablic wynika z tego, �e warto�� tablicy 'result' jest okre�lona w p�tli
                    // i przez to output wyrzuca tak� odpowied�, natomiast nie wiem jak zrobi� z tego tablic� jednowymiarow�.
                }
            }
        }
    }

    public static void main(String[] args) {

        int[] numbers = {1, 2, 4, 7, 8, 10, -1, -2, -4, 100, 22, -10};
        int[] result = Arrays.copyOf(numbers, numbers.length);

        int size = result.length;

        printPairs(result, size);
    }

}
