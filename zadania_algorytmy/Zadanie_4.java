package zadania_algorytmy;

public class Zadanie_4 {

    // Jak usun�� za pomoc� jednego przej�cia po li�cie wszystkie duplikaty, napisz stosowny program

    public static void main(String[] args) {

        int[] numbers = {1, 1, 4, 6, 6, 90, 151, 151, -6, -6};
        for (int i = 0; i < numbers.length; i++) {
            boolean duplicate = false;
            int j = 0;
            while (j < i) {
                if (numbers[i] == numbers[j]) {
                    duplicate = true;
                    j++;
                }
            }
            if (!duplicate) {
                System.out.print(numbers[i] + " ");
            }
        }
    }
}
