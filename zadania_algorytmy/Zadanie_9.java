package zadania_algorytmy;

import java.util.Scanner;

public class Zadanie_9 {

    // Wypisz na ekran N wyraz�w ci�gu Fibonacciego
    // 0,1,1,2,3,5,8,13,21,34... ka�dy kolejny wyraz otrzymujemy poprzez sum� dw�ch poprzednich tj.:
    // 1 = 0 + 1
    // 2 = 1 + 1
    // 3 = 2 + 1
    // 5 = 3 + 2
    // itd...
    private static Scanner sc;

    public static void main(String[] args) {
        System.out.println("Podaj liczb�:");
        sc = new Scanner(System.in);
        int number = sc.nextInt();
        fibonacci(number);
    }

    public static int fibonacci(int n) {
        int num1 = 0;
        int num2 = 1;
        int counter = 0;

        while (counter < n) {
            System.out.print(num1 + " ");
            int num3 = num1 + num2;
            num1 = num2;
            num2 = num3;
            counter++;
        }
        return n;
    }
}
