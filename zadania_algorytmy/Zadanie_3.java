package zadania_algorytmy;

public class Zadanie_3 {

        // Napisz program, który pobiera N liczb całkowitych i wyświetla największą i najmniejszą z nich
        // int[] numbers = {1, 1, 4, 6 ,6 ,90, 151, -6}
        // Najmniejsza podana liczba to -6, a największa to 151

        public static void main(String[] args) {

            int[] numbers = {1, 1, 4, 6, 6, 90, 151, -6};
            int printMax = maxInt(numbers);
            int printMin = minInt(numbers);
            System.out.println("Największa podana liczba to: " + printMax);
            System.out.println("Najmniejsza podana liczba to: " + printMin);
          }

        public static int maxInt (int[] numbers){
            int maxValue = 0;
            for (int i = 0; i < numbers.length; i++){
                if (numbers[i] > maxValue){
                    maxValue = numbers[i];
                }
            }
            return maxValue;
        }

        public static int minInt (int[] numbers){
            int minValue = 0;
            for (int i = 0; i < numbers.length; i++){
                if (numbers[i] < minValue){
                    minValue = numbers[i];
                }
            }
            return minValue;
        }
    }

