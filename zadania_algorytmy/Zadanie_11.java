package zadania_algorytmy;

import java.util.Scanner;

public class Zadanie_11 {

    // Napisz program, kt�ry zamieni liczb� z systemu dziesi�tnego na binarny np. 18 -> 10010

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Program zmieni podan� liczb� z systemu dziesi�tnego na binarny.");
        System.out.println("Podaj liczb�: ");
        int number = sc.nextInt();
        String convert = Integer.toBinaryString(number);
        System.out.println(number + " = " + convert);


    }
}
