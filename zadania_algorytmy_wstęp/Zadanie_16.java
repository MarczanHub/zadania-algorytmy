package zadania_algorytmy_wst�p;

import java.util.Scanner;

public class Zadanie_16 {

    // Napisz metod�, kt�ra ma trzy parametry formalne a, b, c b�d�ce liczbami
    // ca�kowitymi. Funkcja zwraca prawd�, je�li zadane liczby s� liczbami
    // pitagorejskimi oraz fa�sz w przeciwnym wypadku. Liczby pitagorejskie spe�niaj�
    // warunek: a*a+b*b=c*c.

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Program sprawdzi czy wpisane liczy s� liczbami pitagorejskimi.\nWprowad� liczby:");
        System.out.println(isPythagorean(sc.nextInt(), sc.nextInt(), sc.nextInt()));
    }

    public static boolean isPythagorean(int a, int b, int c) {
        return a * a + b * b == c * c;
    }
}

