package zadania_algorytmy_wst�p;

public class Zadanie_10 {

    // Napisz metod�, kt�ra jako argument przyjmuje tablic� String�w. Jako wynik ma
    // zwraca� tablice wszystkich s��w, kt�re zawieraj� s�owa 5-literowe.

    public static void main(String[] args) {
        fiveChars();
    }

    public static String[] fiveChars() {

        String[] array = {"jeden", "dwa", "trzy", "cztery", "pi��", "sze��", "siedem", "osiem"};


        System.out.print("\nS�owa zawieraj�ce 5 liter z tablicy 'array' to:\n");
        for (int i = 0; i < array.length; i++) {
            if (array[i].length() == 5) {
                System.out.print(array[i] + " " + "\n");
            }

        }

        return array;
    }
}


