package zadania_algorytmy_wst�p;

import java.util.Arrays;

public class Zadanie_21 {

    // Napisz metod�, kt�ra zwraca maksymaln� d�ugo�� 2 string�w.
    // maxStringLength(�aaaa�,�sad�) zwr�ci 4.
    // maxStringLength(�aaksadui�,�aaa�) zwr�ci 8.

    public static void main(String[] args) {
        String[] maxStringLength1 = {"aaaa", "sad"};
        String[] maxStringLength2 = {"aaksadui", "aaa"};
        System.out.println("Maksymalna d�ugo�� string�w z tablicy " + Arrays.toString(maxStringLength1) + " wynosi " + maxLength1(maxStringLength1));
        System.out.println("Maksymalna d�ugo�� string�w z tablicy " + Arrays.toString(maxStringLength2) + " wynosi " + maxLength2(maxStringLength2));
    }

    public static int maxLength1(String[] maxStringLength1) {
        int length;
        length = Math.max(maxStringLength1[0].length(), maxStringLength1[1].length());
        return length;
    }

    public static int maxLength2(String[] maxStringLength2) {
        int length;
        length = Math.max(maxStringLength2[0].length(), maxStringLength2[1].length());
        return length;
    }
}
