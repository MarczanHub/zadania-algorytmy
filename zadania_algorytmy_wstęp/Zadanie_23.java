package zadania_algorytmy_wst�p;

import java.util.Scanner;

public class Zadanie_23 {

    // Napisz funkcj�, kt�ra stwierdza, czy zadana jako parametr liczba ca�kowita jest
    // sze�cianem pewnej liczby naturalnej.

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Podaj liczb� ca�kowit�:");
        System.out.println(isCube(sc.nextInt()));
    }

    public static boolean isCube(int number) {

        System.out.println("Podaj sze�cian liczby ca�kowitej:");
        int cubeNumber = sc.nextInt();

        if (number == cubeNumber * cubeNumber * cubeNumber) {
            System.out.println("Podana liczba jest sze�cianem zadanej liczby ca�kowitej.");
            System.exit(0);
        } else {
            System.out.println("Podana liczba nie jest sze�cianem zadanej liczby ca�kowitej.");
            System.exit(0);
        }

        return isCube(sc.nextInt());
    }
}
