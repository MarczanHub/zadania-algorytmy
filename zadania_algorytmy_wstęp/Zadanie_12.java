package zadania_algorytmy_wst�p;

public class Zadanie_12 {

    // Napisz metod�, kt�ra jako argument przyjmuje tablice String�w. Jako wynik
    // metoda ma zwraca� liczb� ca�kowit�, kt�ra oznacza najd�u�szy wyraz w tablicy.

    public static void main(String[] args) {

        System.out.println("\nNajd�u�szy wyraz w tablicy posiada " + longestStringInArray() + " znak�w.");

    }

    public static int longestStringInArray() {
        String[] array = {"VCS", "Edit", "Tools", "Window", "Navigate"};
        int max = 0;
        int longestString = array[0].length();
        for (int i = 0; i < array.length; i++) {
            if (array[i].length() > longestString) {
                max = i;
                longestString = array[i].length();
            }
        }
        return array[max].length();
    }
}
