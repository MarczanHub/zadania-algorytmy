package zadania_algorytmy_wst�p;

import java.util.Scanner;

public class Zadanie_26 {

    // Napisz metod�, kt�ra sprawdza czy dany wyraz jako argument jest palindromem.

    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {

        System.out.println("Podaj wyraz:");
        System.out.println(isPalindrome());
    }

    public static boolean isPalindrome (){
        String inputWord = sc.nextLine();
        for (int i = 0; i < inputWord.length(); i++) {
            if (inputWord.charAt(i) != inputWord.charAt(inputWord.length() - 1 - i)){
                return false;
            }
        }
        return true;
    }
}
