package zadania_algorytmy_wst�p;

import java.util.Arrays;

public class Zadanie_11 {

    // Napisz metod�, kt�ra jako argument przyjmuje tablic� String�w. Jako wynik ma
    // zwraca� tablice tej samej d�ugo�ci, w kt�rej wyrazy s� napisane ma�ymi literami
    // (du�e litery zamieniamy na ma�e).

    public static void main(String[] args) {
        System.out.println(toLowerCaseString());
    }

    public static String toLowerCaseString() {

        String[] array = {"FILE", "EDIT", "VIEW", "NAVIGATE", "CODE"};
        String printStrings = Arrays.toString(array);
        for (int i = 0; i < array.length; i++) {
            if (printStrings.equals(printStrings.toUpperCase())) {
                printStrings = printStrings.toLowerCase();
            }
        }
        return printStrings;
    }
}


