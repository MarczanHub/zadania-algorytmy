package zadania_algorytmy_wst�p;

import java.util.Arrays;

public class Zadanie_15 {

    // Napisz metod�, kt�ra wyznaczy liczb� wyst�pie� liczby k w danym ci�gu.
    // numberOfElements([1,2,3,3,3,4,3],3) = 4 ( 3 wyst�puje 4 razy w danym ci�gu).
    // numberOfElements([1,2,3,3,3,4,3],8) = 0 ( 8 nie wyst�puje ani razu w danym ci�gu).

    public static void main(String[] args) {

        int[] numberOfElements = {1, 2, 3, 3, 3, 4, 3};
        numberThree(numberOfElements);
        System.out.println(Arrays.toString(numberOfElements) + " = " + numberThree(numberOfElements));
        System.out.println(Arrays.toString(numberOfElements) + " = " + numberEight(numberOfElements));
    }

    public static int numberThree(int[] numberOfElements) {

        int countThree = 0;
        for (int i = 0; i < numberOfElements.length; i++) {
            if (numberOfElements[i] == 3) {
                countThree++;
            }
        }
        return countThree;
    }

    public static int numberEight(int[] numberOfElements) {

        int countEight = 0;
        for (int i = 0; i < numberOfElements.length; i++) {
            if (numberOfElements[i] == 8) {
                countEight++;
            }
        }
        return countEight;
    }
}
