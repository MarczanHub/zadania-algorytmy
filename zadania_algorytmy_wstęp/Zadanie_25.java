package zadania_algorytmy_wst�p;

public class Zadanie_25 {

    // Skorzystaj z wcze�niejszej funkcji (zad.11.) do znalezienia liczby od 2 do 10000,
    // kt�ra ma najwi�ksz� liczb� dzielnik�w.

    public static void main(String[] args) {

        countMaxDivisor();
    }

    public static void countMaxDivisor() {

        int numberOfDivisors;
        int maxDivisors = 1;           // zak�adaj�c, �e minimalna liczba dzielnik�w wynosi 1
        int numberWithMaxDivisors = 1;

        for (numberOfDivisors = 2; numberOfDivisors <= 10000; numberOfDivisors++) {

            int number;
            int divisorCount = 0;

            for (number = 2; number <= numberOfDivisors; number++) {

                if (numberOfDivisors % number == 0) {
                    divisorCount++;

                } else if (divisorCount > maxDivisors) {

                    maxDivisors = divisorCount;
                    numberWithMaxDivisors = numberOfDivisors;

                }
            }
        }

        System.out.println("Maksymalna liczba wszystkich dzielnik�w z przedzia�u od 2 do 10000 wynosi: " + maxDivisors);
        System.out.println("Liczba posiadaj�ca " + maxDivisors + " dzielnik�w to " + numberWithMaxDivisors);
    }
}
