package zadania_algorytmy_wst�p;

import java.util.Scanner;

public class Zadanie_22 {

    // Napisz funkcj�, kt�ra odczytuje jako argument liczb� i wypisuje liczb� dzielnik�w.

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Podaj liczb�: ");
        countAllDivisors();
    }

    public static int countAllDivisors() {

        int number = sc.nextInt();
        int numberOfDivisors = 0;
        System.out.println("Liczba wszystkich dzielnik�w podanej liczy wynosi: ");

        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                numberOfDivisors++;
            }
        }
        System.out.println(numberOfDivisors);
        return number;
    }
}
