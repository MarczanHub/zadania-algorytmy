package zadania_algorytmy_wst�p;


import java.util.Arrays;

public class Zadanie_6 {
    //Napisz metod�, kt�ra odwraca dan� tablic� liczb ca�kowitych.
    //swap([1,2,3]) = [3,2,1]
    public static void main(String[] args) {

    swap();

    }

    public static int swap() {
        int[] array = {1, 2, 3};
        System.out.println("\nTablica odwr�ci wszystkie zawarte w niej liczby ca�kowite:");
        System.out.print(Arrays.toString(array) + " = ");
        for (int i = 0; i < array.length; i++){
            array[i] = 3 - i;
        }
        System.out.print(java.util.Arrays.toString(array) + "\n");
        System.exit(0);
        return swap();
    }
}