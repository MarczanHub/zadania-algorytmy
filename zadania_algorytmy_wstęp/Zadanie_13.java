package zadania_algorytmy_wst�p;


import java.util.Arrays;

public class Zadanie_13 {

    // Napisz metod�, kt�ra jako argument przyjmuje tablic� liczb. Jako wynik metoda
    // powinna zwr�ci� tablic� wszystkich liczb, kt�re maj� dok�adnie dwa dzielniki.

    public static void main(String[] args) {

        System.out.println("Liczby posiadaj�ce dok�adnie dwa dzielniki to: ");
        twoDivisorArray();
    }

    public static void twoDivisorArray() {
        int[] array = {2, 3, 5, 7, 6, 9};
        boolean isPrime;
        for (int i = 0; i < array.length; i++) {
            isPrime = true;

            for (int j = 2; j < array[i]; j++) {
                if (array[i] % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                System.out.println(array[i]);
            }
        }
    }
}
