package zadania_algorytmy_wst�p;

import java.util.Arrays;

public class Zadanie_7 {

    //Co z tym kodem jest nie tak?
    //int[] array = new int[5];
    //for (int i=0;i <=5;i++){
    //array[i] =12;
    //}

    public static void main(String[] args) {

        // U�ycie operatora "<=" zwr�ci nam b��d "index out of bounds" - wynika to z tego,
        // �e pr�bujemy si� odwo�a� do pi�tego index-u, a liczenie index-�w zaczynamy od zera,
        // zatem ostatnim elementem tablicy b�dzie index czwarty - array[4].

        //Poprawnie napisany kod powinien wygl�da� tak jak ten poni�ej:

        int[] array = new int[5];
        for (int i = 0; i < 5; i++){
            array[i] = 12;
            System.out.println(Arrays.toString(array)); // To jest oczywi�cie opcjonalne wywo�anie tablicy w terminalu,
                                                        // �eby sprawdzi� poprawno�� kodu.
        }
    }
}
