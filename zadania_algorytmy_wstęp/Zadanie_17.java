package zadania_algorytmy_wst�p;

import java.util.Scanner;

public class Zadanie_17 {

    // Napisz metod� sprawdzaj�c�, czy liczba jest parzysta.

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Program sprawdzi czy podana liczba jest parzysta.\nPodaj liczb�:");
        int number = sc.nextInt();
        System.out.println(checkIfNumberIsEven(number));
    }

    public static boolean checkIfNumberIsEven(int number) {
        return number % 2 == 0;
    }
}
