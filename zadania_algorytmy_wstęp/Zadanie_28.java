package zadania_algorytmy_wst�p;

import java.util.Arrays;

public class Zadanie_28 {

    // Napisz program, kt�ry odwr�ci tablic� int-�w
    // int[] numbers = {1,2,45,5,6,7,88,100}
    // int[] result = {100,88,7,6,5,45,2,1}

    public static void main(String[] args) {

        int[] numbers = {1, 2, 45, 5, 6, 7, 88, 100};
        System.out.println("int[] numbers = " + Arrays.toString(numbers));
        reverseArray(numbers, numbers.length);

    }

    public static void reverseArray(int[] numbers, int arraySize) {
        int[] result = Arrays.copyOf(numbers, arraySize);
        for (int i = 0; i < arraySize / 2; i++) {
            result[i] = result[arraySize - i - 1];
        }
        System.out.println("int[] result = " + Arrays.toString(result));
    }
}
