package zadania_algorytmy_wst�p;

import java.util.Scanner;

public class Zadanie_19 {

    // Napisz funkcj�, kt�ra stwierdza, czy zadana jako parametr liczba ca�kowita jest
    // kwadratem pewnej liczby ca�kowitej. Liczby b�d�ce kwadratami liczb ca�kowitych
    // to 1, 4, 9, 16 itd. Warto�ci� funkcji ma by� prawda, je�li liczba spe�nia warunek oraz
    // fa�sz w przeciwnym wypadku.

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Wprowad� liczb� ca�kowit�:");
        System.out.println(isSquareNumber(sc.nextInt()));


    }

    public static boolean isSquareNumber(int number) {

        System.out.println("Podaj kwadrat liczby ca�kowitej:");
        int squareNumber = sc.nextInt();
        if (number == squareNumber * squareNumber) {
            System.out.println(true);
            System.exit(0);
        } else {
            System.out.println(false);
            System.exit(0);
        }
        return isSquareNumber(sc.nextInt());
    }
}
