package zadania_algorytmy_wst�p;

import java.util.Arrays;

public class Zadanie_8 {

    // Napisz metod�, kt�ra zwraca posortowan� tablic� liczb.
    // mySort([4,1,9,15]) = [1,4,9,15]

    public static void main(String[] args) {

        System.out.print(Arrays.toString(mySort()));
    }

    public static int[] mySort(){

        int[] array = {4,1,9,15};
        System.out.print("Tablica " + Arrays.toString(array) + " po przesortowaniu zwraca warto�ci: ");
        Arrays.sort(array);

        return array;
    }

}
