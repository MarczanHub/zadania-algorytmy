package zadania_algorytmy_wst�p;

import java.util.Arrays;

public class Zadanie_14 {

    // Napisz metod�, kt�ra jako argument zawiera tablic� element�w typu boolean.
    // Metoda ma zwr�ci� now� tablic�, kt�ra zawiera tyle element�w true ile znajduje
    // si� w bazowej tablicy.
    // newArray([false,true,true,false,false]) = [true,true]
    // newArray([true,true,true,true,false,false, true]) = [true,true,true,true,true]

    public static void main(String[] args) {

        boolean[] newArray = {false, true, true, false, false};
        boolean[] secondArray = {true,true,true,true,false,false,true};

      returnTrueValue(newArray);
      replaceFalseValue(secondArray);
    }

    public static boolean returnTrueValue(boolean[] newArray) {
        for (int i = 0; i < newArray.length; i++) {
            if (newArray[i]) {
               boolean[] array = new boolean[]{newArray[i], newArray[i]};
                System.out.println(Arrays.toString(newArray) + " = " + Arrays.toString(array));
                break;
            }
            else {
                continue;
            }
        }
            return true;
    }

    public static boolean replaceFalseValue(boolean[] secondArray){

        for (int i = 0; i < secondArray.length; i++) {
            if (secondArray[i]){
               secondArray[i] = true;
                boolean[] array = new boolean[]{secondArray[i],secondArray[i],secondArray[i],secondArray[i],secondArray[i]};
                System.out.print(Arrays.toString(secondArray) + " = " + Arrays.toString(array));
                break;

            } else {
                continue;
            }
        }
        return true;
    }
}
