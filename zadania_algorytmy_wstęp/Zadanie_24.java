package zadania_algorytmy_wst�p;

import java.util.Scanner;

public class Zadanie_24 {

    // Napisz funkcj�, kt�ra stwierdza, czy zadana jako parametr liczba ca�kowita jest
    // liczb� pierwsz�.

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Podaj liczb� ca�kowit�: ");
        int number = sc.nextInt();
        boolean isPrime = isPrimeNumber(number);
        if (isPrime){
            System.out.println("Liczba " + number + " jest liczb� pierwsz�.");
        } else {
            System.out.println("Liczba " + number + " nie jest liczb� pierwsz�.");
        }


    }

    public static boolean isPrimeNumber(int number) {

        boolean isPrime = true;
        if (number <= 1) {
            isPrime = false;
            return isPrime;
        } else {
            for (int i = 2; i <= number / 2; i++) {
                if (number % i == 0) {
                    isPrime = false;
                    return isPrime;
                }
                break;
            }
        }
        return isPrime;
    }
}