package zadania_algorytmy_wst�p;

import java.util.Scanner;

public class Zadanie_27 {

    // Napisz metod�, kt�ra sprawdza, czy wyraz jest peselem. Dla uproszczenia
    // sprawdzamy, czy ka�dy znak jest cyfr� i czy wyraz ma 11 znak�w.
    // isPesel (�87122508076�)

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Podaj pesel: ");
        String pesel = sc.nextLine();
        boolean isPesel = isPesel(pesel);
        if (isPesel) {
            System.out.println("Podany wyraz jest peselem");
        } else {
            System.out.println("Podany wyraz nie jest peselem");
        }
    }

    public static boolean isPesel(String pesel) {

        for (int i = 0; i < pesel.length(); i++) {

            if (pesel.length() == 11 && pesel.matches("[0-9]+")) {
                return true;
            } else {
                return false;
            }
        }
        return isPesel(pesel);
    }
}
